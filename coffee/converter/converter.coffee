class MivaConverter
    constructor: () ->
        subscribe 'submit.mivaform', _.bind(@onSubmit, @)


    onSubmit: (text) ->
        @convert text


    convert: (text) ->
        words = text.split(' ')
        convertedWords = _.map words, @convertWord, @
        converted = convertedWords.join ' '
        publish 'convert.mivaconverter', [converted]


    convertWord: (word) ->
        convertedWord = new MivaConverterWord(word)
        convertedWord.get()
