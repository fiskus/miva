class MivaConverterLetter
    REGEX:
        VOWELS_ALL: /[АЕЁИОУЭЮЯаеёиоуэюя]/g
        VOWELS_SMALL: /[аеёиоуэюя]/g
        VOWELS_BIG: /[АЕЁИОУЭЮЯ]/g


    SINGLE_REPLACERS:
            'И': 'I'
            'и': 'i'
            'С': 'З'
            'с': 'з'
            'В': 'У'
            'в': 'у'
            'У': 'В'
            'у': 'в'


    REPLACERS:
        COMMON:
            'И': 'I'
            'и': 'i'
            'Ё': 'I'
            'ё': 'i'
            'О': 'I'
            'о': 'i'
            'Э': 'I'
            'э': 'i'
            'Ю': 'I'
            'ю': 'i'
            'Я': 'I'
            'я': 'i'

            'Е': 'Є'
            'е': 'є'
            'Г': 'Ґ'
            'г': 'ґ'
            'Й': 'Ї'
            'й': 'ї'
            'Ы': 'И'
            'ы': 'и'
            'ь': "'"
            'ъ': "'"
        'т':
            'ь': "и"
            'ъ': "и"
        'з':
            'ы': 'у'
        #TODO: заведения → заведення, организм → органiзм
        #'н':
        #    'и': "н"


    LAST_REPLACERS:
        COMMON:
            'Е': 'Є'
            'е': 'є'
            'Г': 'Ґ'
            'г': 'ґ'
            'Й': 'Ї'
            'й': 'ї'
            'Ы': 'И'
            'ы': 'и'
            'л': 'в'
            'ь': "'"
            'ъ': "'"
        'т':
            'ь': "и"
            'ъ': "и"
        'ц':
            'а': 'я'
            'А': 'Я'
        'е':
            'т': ' '


    constructor: (letter, index, size, prev, next) ->
        @_letter = letter
        @_index = index + 1
        @_size = size
        @_prev = prev
        @_next = next

        @convert()


    convert: () ->
        if @_size == 1
            @convertSingleLetter()
            return

        if @_index == 1
            @convertFirstLetter()
        else if @_index == @_size
            @convertLastLetter()
        else
            if @_prev and @REPLACERS[@_prev]
                replacer = @REPLACERS[@_prev][@_letter]
                if !replacer
                    replacer = @REPLACERS.COMMON[@_letter]
            else
                replacer = @REPLACERS.COMMON[@_letter]
            if replacer
                @_letter = replacer

    convertFirstLetter: () ->
        replacer = @REPLACERS.COMMON[@_letter]

        if @_letter.match @REGEX.VOWELS_SMALL
            if replacer
                @_letter = 'в' + replacer
            else
                @_letter = 'в' + @_letter
        else if @_letter.match @REGEX.VOWELS_BIG
            if replacer
                @_letter = 'в' + replacer.toLowerCase()
            else
                @_letter = 'в' + @_letter.toLowerCase()

    convertLastLetter: () ->
        if @_prev and @LAST_REPLACERS[@_prev]
            replacer = @LAST_REPLACERS[@_prev][@_letter]
            if !replacer
                replacer = @LAST_REPLACERS.COMMON[@_letter]
        else
            replacer = @LAST_REPLACERS.COMMON[@_letter]
        if replacer
            @_letter = replacer


    get: () ->
        @_letter

    convertSingleLetter: () ->
        replacer = @SINGLE_REPLACERS[@_letter]
        if replacer
            @_letter = replacer
