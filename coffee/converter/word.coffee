class MivaConverterWord
    PUNCTUATION: ['.', ',', ';', ':', '!', '?']

    constructor: (word) ->
        @_word = word
        @collectMeta()
        @convert()


    collectMeta: () ->
        @_size = @_word.length


    get: () ->
        @_word


    convert: () ->

        @_letters = @_word.split ''

        lastLetter = _.last @_letters
        if _.indexOf(@PUNCTUATION, lastLetter) > -1
            @_letters = _.initial @_letters
            @_size--
        else
            lastLetter = ''

        convertedLetters = _.map @_letters, @convertLetter, @
        if lastLetter
            convertedLetters.push lastLetter

        @_word = convertedLetters.join ''


    convertLetter: (letter, index) ->
        prev = ''
        next = ''
        if index > 0
            prev = @_letters[index - 1]
        if index < @_size - 1
            next = @_letters[index + 1]
        convertedLetter = new MivaConverterLetter(letter, index, @_size, prev, next)
        convertedLetter.get()
