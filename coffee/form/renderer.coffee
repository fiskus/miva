class MivaFormRenderer
    SELECTORS:
        WRAPPER: '.miva-form-wrapper'
        FORM: '.miva-form'
        INPUT: '.miva-input'


    TEMPLATE: JST['handlebars/form.hbs']


    constructor: () ->
        subscribe 'load.mivasamplesmodel', _.bind(@onLoad, @)
        @render()


    render: () ->
        wrapper = getElement @SELECTORS.WRAPPER
        html = @TEMPLATE()
        wrapper.innerHTML = html
        @onRender()


    onRender: () ->
        @_input = getElement @SELECTORS.INPUT
        @_form = getElement @SELECTORS.FORM
        listen @_form, 'keyup', @onKey, @
        listen @_form, 'submit', @onSubmit, @


    onSubmit: (event) ->
        event.preventDefault()
        @_text = @_input.value
        publish 'submit.mivaform', [@_text]


    onLoad: (text) ->
        @_input.value = text


    onKey: (event) ->
        text = @_input.value
        onTimeout = () ->
            @onTimeout(text)
        setTimeout _.bind(onTimeout, @), 500
        @_text = text


    onTimeout: (text) ->
        if @_text == text
            publish 'submit.mivaform', [@_text]
