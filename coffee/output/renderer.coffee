class MivaOutputRenderer
    SELECTORS:
        WRAPPER: '.miva-output-wrapper'


    TEMPLATE: JST['handlebars/output.hbs']


    constructor: () ->
        subscribe 'convert.mivaconverter', _.bind(@onConvert, @)


    onConvert: (text) ->
        @_text = text
        @render()


    render: () ->
        wrapper = getElement @SELECTORS.WRAPPER
        html = @getHtml()
        wrapper.innerHTML = html


    getHtml: () ->
        data =
            TEXT: @_text
        @TEMPLATE data
