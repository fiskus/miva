class MivaSamplesModel
    URL: '/miva/samples/{page}.json'
    SIZE: 2126


    constructor: () ->
        subscribe 'click.mivasamplesrenderer', _.bind(@onName, @)


    onName: (name) ->
        if name == 'random'
            random = @getRandom()
            @load('txtdump/' + random)
        else
            @load name


    load: (name) ->
        url = @URL.replace '{page}', name
        ajaxGet url, {}, @onLoad, @


    onLoad: (data) ->
        publish 'load.mivasamplesmodel', [data.post]


    getRandom: () ->
        (Math.random() * 2126).toFixed()
