class MivaSamplesRenderer
    SELECTORS:
        WRAPPER: '.miva-samples-wrapper'
        LINK: '.miva-sample-inner'


    TEMPLATE: JST['handlebars/samples.hbs']


    constructor: () ->
        @render()


    render: () ->
        @_wrapper = getElement(@SELECTORS.WRAPPER)
        html = @TEMPLATE()
        @_wrapper.innerHTML = html
        @onRender()


    onRender: () ->
        links = getElements @SELECTORS.LINK
        listen links, 'click', @onClick, @


    onClick: (event) ->
        link = event.currentTarget
        name = link.getAttribute 'data-name'
        publish 'click.mivasamplesrenderer', [name]
