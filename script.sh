perl-rename 's/\#(\d\d?\d?\d?).*.txt/$1\.json/' *.txt
find . -name "*.json" -print | xargs sed -i ':a;N;$!ba;s/\n/\\n/g'
find . -name "*.json" -print | xargs sed -i 's/"/\\"/g'
find . -name "*.json" -print | xargs sed -i 's/^/{"post": "/g'
find . -name "*.json" -print | xargs sed -i 's/$/"}/g'
